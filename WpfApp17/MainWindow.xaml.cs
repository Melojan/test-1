﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfApp17.View;

namespace WpfApp17
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static int IdAuthor { get; set; }
        public static int IdPublish { get; set; }
        public static int IdBook { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Book_Click(object sender, RoutedEventArgs e)
        {
            WindowBook wBook = new WindowBook();
            wBook.Show();
        }

        private void Author_Click(object sender, RoutedEventArgs e)
        {
            WindowAuthor wAuthor = new WindowAuthor();
            wAuthor.Show();
        }

        private void Publish_Click(object sender, RoutedEventArgs e)
        {
            WindowPublish wPublish = new WindowPublish();
            wPublish.Show();
        }
    }
}
