﻿namespace WpfApp17.Model
{
    public class Author
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Author() { }

        public Author(int id, string firstName, string lastName)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        public Author ShallowCopy()
        {
            return (Author)this.MemberwiseClone();
        }
    }
}
