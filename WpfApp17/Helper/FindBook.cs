﻿using WpfApp17.Model;

namespace WpfApp17.Helper
{
    class FindBook
    {
        int id;

        public FindBook(int id)
        {
            this.id = id;
        }

        public bool BookPredicate(Book book)
        {
            return book.Id == id;
        }
    }
}
