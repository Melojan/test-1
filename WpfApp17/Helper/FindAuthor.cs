﻿using WpfApp17.Model;

namespace WpfApp17.Helper
{
    public class FindAuthor
    {
        int id;

        public FindAuthor(int id)
        {
            this.id = id;
        }

        public bool AuthorPredicate(Author author)
        {
            return author.Id == id;
        }
    }
}
